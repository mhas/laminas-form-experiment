import Vue from 'vue'
import VueRouter from 'vue-router'
import Form from "@/views/Form";
import Client from "@/views/Client";
import NotFound from "@/views/error/NotFound";

Vue.use(VueRouter)

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'form',
            component: Form,
        },
        {
            path: '/client/:id',
            name: 'client',
            component: Client,
        },
        {
            path: '/404',
            name: '404',
            component: NotFound ,
            props: true
        },
        {
            path: '*',
            redirect: { name: '404', params: { code: '404' } }
        }
    ]
})

export default router
