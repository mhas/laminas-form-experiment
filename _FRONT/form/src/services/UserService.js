import api from "../utils/api";

export default {
    getUsers() {
        return api.get("/user");
    },
    getUserById(id) {
        return api.get(`/user/${id}`);
    },
    postUser(user) {
        return api.post("/user", user);
    },
    putUser(user) {
        return api.put(`/user/${user.id}`, user);
    },
    deleteUser(id) {
        return api.delete(`/user/${id}`);
    },
};