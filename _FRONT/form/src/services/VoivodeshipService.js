import api from "../utils/api";

export default {
    getVoivodeship() {
        return api.get("/voivodeship");
    },
};