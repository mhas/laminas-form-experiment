import axios from "axios";

const api = axios.create({
    baseURL: process.env.VUE_APP_API_URI ?? process.env.VUE_APP_API_URI,
    headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
    },
});

api.interceptors.response.use((res) => {
    return Promise.resolve(res);
}, (error) => {
    if (error?.response?.status === 401) {
        console.error(error);
    }
    if (error?.response?.status === 422) {
        return Promise.reject(error);
    }

    return Promise.reject(error);
});

export default api;
