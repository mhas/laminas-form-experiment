module.exports = {
    outputDir: "../../public/app/",
    publicPath: "/app",
    devServer: {
        watchOptions: {
            poll: true
        }
    }
}