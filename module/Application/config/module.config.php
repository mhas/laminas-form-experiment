<?php

declare(strict_types=1);

namespace Application;

use Application\Factory\UserControllerFactory;
use Application\Factory\UserServiceFactory;
use Application\Factory\VoivodeshipCommandFactory;
use Application\Factory\VoivodeshipControllerFactory;
use Application\Factory\VoivodeshipServiceFactory;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use Laminas\ServiceManager\Factory\InvokableFactory;

return [
    'router' => [
        'routes' => [
            'api' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/api',
                ],
                'may_terminate' => false,
                'child_routes' => [
                    'v1' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/v1',
                        ],
                        'may_terminate' => false,
                        'child_routes' => [
                            'user' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/user[/:id]',
                                    'defaults' => [
                                        'controller' => Controller\Api\UserController::class,
                                    ],
                                ],
                            ],
                            'voivodeship' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/voivodeship[/:id]',
                                    'defaults' => [
                                        'controller' => Controller\Api\VoivodeshipController::class,
                                    ],
                                ],
                            ],
                        ],
                    ]
                ]
            ],
            'user' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'controller' => Controller\UserController::class,
                        'action'     => 'form',
                    ],
                ],
            ],
            'application' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/application[/:action]',
                    'defaults' => [
                        'controller' => Controller\Api\UserController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\Api\UserController::class => UserControllerFactory::class,
            Controller\Api\VoivodeshipController::class => VoivodeshipControllerFactory::class,
            Controller\UserController::class => InvokableFactory::class,
        ],
    ],
    'service_manager' => [
        'factories' => [
            Service\UserService::class => UserServiceFactory::class,
            Service\VoivodeshipService::class => VoivodeshipServiceFactory::class,
            Listener\ErrorListener::class => InvokableFactory::class,
            Command\VoivodeshipCommand::class => VoivodeshipCommandFactory::class,
        ]
    ],
    'input_filters' => [
        'factories' => [
            InputFilter\UserInputFilter::class => InvokableFactory::class,
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ]
            ]
        ]
    ],
    'laminas-cli' => [
        'commands' => [
            'get-voivodeships'  => Command\VoivodeshipCommand::class
        ],
    ],
];
