<?php

declare(strict_types=1);

namespace Application\Command;

use Application\Service\VoivodeshipService;
use Laminas\Http\Client;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class VoivodeshipCommand extends Command
{
    protected static $defaultName = 'get-voivodeships';

    private $voivodeshipService;

    private $httpClient;

    public function __construct(VoivodeshipService $voivodeshipService, Client $client)
    {
        parent::__construct(self::$defaultName);

        $this->voivodeshipService = $voivodeshipService;
        $this->httpClient = $client;
    }

    protected function execute(InputInterface $input, OutputInterface $output) : int
    {
        $response = $this->httpClient->send();

        if ($response->getStatusCode() !== 200) {
            return 1;
        }

        $data = $response->getBody();

        $this->voivodeshipService->synchronize(json_decode($data));
        $output->writeln('Pobrano: ' . $data);
        return 0;
    }
}