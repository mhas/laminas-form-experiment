<?php

declare(strict_types=1);

namespace Application\Controller\Api;

use Application\InputFilter\UserInputFilter;
use Application\Service\UserService;
use Laminas\Mvc\Controller\AbstractRestfulController;
use Laminas\View\Model\JsonModel;
use Laminas\View\Model\ViewModel;

class UserController extends AbstractRestfulController
{
    private $userService;

    private $userInputFilter;

    public function __construct(UserService $userService, UserInputFilter $userInputFilter)
    {
        $this->userService = $userService;
        $this->userInputFilter = $userInputFilter;
    }

    private function isCompany(array $data): bool {
        return (bool) $data['company'] ?? false;
    }

    public function get($id): JsonModel
    {
        return new JsonModel([
            'status' => 'success',
            'code' => 200,
            'data' => $this->userService->getById((int) $id)
        ]);
    }

    public function getList(): JsonModel
    {
        return new JsonModel([
            'status' => 'success',
            'code' => 200,
            'data' =>  $this->userService->getList()
        ]);
    }

    public function create($data): JsonModel
    {
        $this->userInputFilter->detectRegistrationStrategy($this->isCompany($data));
        $this->userInputFilter->setData($data);

        if (!$this->userInputFilter->isValid())
        {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel([
                'status' => 'error',
                'code' => 400,
                'messages' => $this->userInputFilter->getMessages()
            ]);
        }

        $filteredData = $this->userInputFilter->getValues();

        $this->userService->save(
            $filteredData['name'],
            $filteredData['email'],
            $filteredData['voivodeship'],
            $filteredData['street'],
            $filteredData['houseNumber'],
            $filteredData['city'],
            $filteredData['postalCode'],
            $filteredData['phonePrefix'],
            $filteredData['phoneNumber'],
            $filteredData['pesel'] ?? null,
            $filteredData['nip'] ?? null
        );

        $this->getResponse()->setStatusCode(201);
        return new JsonModel([
            'status' => 'created',
            'code' => 201,
        ]);
    }

    public function update($id, $data): JsonModel
    {
        $this->userInputFilter->detectRegistrationStrategy($this->isCompany($data));
        $this->userInputFilter->setData(array_merge($data, ['id' => $id]));

        if (!$this->userInputFilter->isValid())
        {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel([
                'status' => 'not updated',
                'code' => 400,
                'messages' => $this->userInputFilter->getMessages()
            ]);
        }

        $filteredData = $this->userInputFilter->getValues();

        $this->userService->update(
            (int) $id,
            $filteredData['name'],
            $filteredData['email'],
            $filteredData['voivodeship'],
            $filteredData['street'],
            $filteredData['houseNumber'],
            $filteredData['city'],
            $filteredData['postalCode'],
            $filteredData['phonePrefix'],
            $filteredData['phoneNumber'],
            $filteredData['pesel'] ?? null,
            $filteredData['nip'] ?? null
        );

        $this->getResponse()->setStatusCode(200);
        return new JsonModel([
            'status' => 'success',
            'code' => 201
        ]);
    }

    public function delete($id): JsonModel
    {
        $this->userService->delete((int) $id);

        $this->getResponse()->setStatusCode(200);
        return new JsonModel([
            'status' => 'success',
            'code' => 201,
        ]);
    }
}
