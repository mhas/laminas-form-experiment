<?php

declare(strict_types=1);

namespace Application\Controller\Api;

use Laminas\Http\Client;
use Laminas\Mvc\Controller\AbstractRestfulController;
use Laminas\Uri\Uri;
use Laminas\View\Model\JsonModel;

class VoivodeshipController extends AbstractRestfulController
{
    private $httpClient;

    public function __construct(Client $client)
    {
        $this->httpClient = $client;
    }

    public function getList(): JsonModel
    {
        $response = $this->httpClient->send();

        if ($response->getStatusCode() !== 200)
        {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel([
                'status' => 'Something is wrong',
                'code' => 400,
            ]);
        }

        return new JsonModel([
            'status' => 'ok',
            'code' => 200,
            'data' =>  json_decode($response->getBody())
        ]);
    }
}