<?php

namespace Application\Controller;

use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;

class UserController extends AbstractActionController
{
    public function formAction(): ViewModel
    {
        $this->redirect()->toUrl('/app');

        return new ViewModel();
    }
}