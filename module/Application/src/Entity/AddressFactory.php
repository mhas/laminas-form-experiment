<?php

namespace Application\Entity;

class AddressFactory
{
    public function create(
        ?string $voivodeship = null,
        ?string $street = null,
        ?string $houseNumber = null,
        ?string $city = null,
        ?string $postalCode = null
    ): Address
    {
        return new Address(
            $voivodeship,
            $street,
            $houseNumber,
            $city,
            $postalCode,
        );
    }
}