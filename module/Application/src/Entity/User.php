<?php

declare(strict_types=1);

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="Application\Repository\UserRepository")
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="string", unique=true)
     */
    private $email;

    /**
     * @ORM\OneToOne(targetEntity="Address", cascade={"persist", "remove"})
     */
    private $address;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $phonePrefix;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $phoneNumber;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $pesel;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $nip;


    public function __construct(
        string $name,
        string $email,
        ?Address $address,
        ?string $phonePrefix,
        ?string $phoneNumber,
        ?string $pesel,
        ?string $nip
    )
    {
        $this->name = $name;
        $this->email = $email;
        $this->address = $address;
        $this->phonePrefix = $phonePrefix;
        $this->phoneNumber = $phoneNumber;
        $this->pesel = $pesel;
        $this->nip = $nip;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function getAddress(): ?Address
    {
        return $this->address;
    }

    public function setAddres(?Address $address): void
    {
        $this->address = $address;
    }

    public function getPhonePrefix(): ?string
    {
        return $this->phonePrefix;
    }

    public function setPhonePrefix(?string $phonePrefix): void
    {
        $this->phonePrefix = $phonePrefix;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(?string $phoneNumber): void
    {
        $this->phoneNumber = $phoneNumber;
    }

    public function getPesel(): ?string
    {
        return $this->pesel;
    }

    public function setPesel(?string $pesel): void
    {
        $this->pesel = $pesel;
    }

    public function getNip(): ?string
    {
        return $this->nip;
    }

    public function setNip(?string $nip): void
    {
        $this->nip = $nip;
    }
}
