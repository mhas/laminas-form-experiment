<?php

namespace Application\Entity;

class UserFactory
{
    public function create(
        string  $name,
        string  $email,
        ?string $voivodeship,
        ?string $street,
        ?string $houseNumber,
        ?string $city,
        ?string $postalCode,
        ?string $phonePrefix,
        ?string $phoneNumber,
        ?string $pesel,
        ?string $nip
    ): User
    {
        $address = (new AddressFactory())->create(
            $voivodeship,
            $street,
            $houseNumber,
            $city,
            $postalCode
        );

        return new User(
            $name,
            $email,
            $address,
            $phonePrefix,
            $phoneNumber,
            $pesel,
            $nip
        );
    }
}