<?php

declare(strict_types=1);

namespace Application\Factory;

use Application\Controller\Api\UserController;
use Application\InputFilter\UserInputFilter;
use Application\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class UserControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null): UserController
    {
        return new UserController(
            $container->get(UserService::class),
            $container->get('InputFilterManager')->get(UserInputFilter::class)
        );
    }

}
