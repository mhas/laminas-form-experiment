<?php

declare(strict_types=1);

namespace Application\Factory;

use Application\Entity\User;
use Application\Service\UserService;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class UserServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null): UserService
    {
        return new UserService(
            $container->get(EntityManager::class)->getRepository(User::class)
        );
    }

}
