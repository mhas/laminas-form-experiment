<?php

declare(strict_types=1);

namespace Application\Factory;

use Application\Controller\Api\VoivodeshipController;
use Interop\Container\ContainerInterface;
use Laminas\Http\Client as HttpClient;
use Laminas\ServiceManager\Factory\FactoryInterface;

class VoivodeshipControllerFactory implements FactoryInterface
{
    const VOIVODESHIP_URI = "http://api.dro.nazwa.pl";

    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null): VoivodeshipController
    {
        $client = new HttpClient(
            self::VOIVODESHIP_URI,
            [
                'adapter'     => HttpClient\Adapter\Curl::class,
                'maxredirects' => 0,
                'timeout'      => 30,
                'curloptions' => [
                    CURLOPT_RETURNTRANSFER => 1
                ],
            ]);

        return new VoivodeshipController(
            $client
        );
    }

}
