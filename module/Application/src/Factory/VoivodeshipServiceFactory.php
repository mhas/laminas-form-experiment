<?php

declare(strict_types=1);

namespace Application\Factory;

use Application\Entity\Voivodeship;
use Application\Service\UserService;
use Application\Service\VoivodeshipService;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class VoivodeshipServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null): VoivodeshipService
    {
        return new VoivodeshipService(
            $container->get(EntityManager::class)->getRepository(Voivodeship::class)
        );
    }

}
