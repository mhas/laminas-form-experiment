<?php

declare(strict_types=1);

namespace Application\Repository;

use Application\Entity\Address;
use Application\Entity\User;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    public function findOneById(int $id): array
    {
        return $this->createQueryBuilder('u')
            ->select('u', 'a')
            ->leftJoin('u.address', 'a')
            ->where('u.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getSingleResult(AbstractQuery::HYDRATE_ARRAY);
    }

    public function findAll(): array
    {
        return $this->createQueryBuilder('u')
            ->select('u', 'a')
            ->leftJoin('u.address', 'a')
            ->getQuery()
            ->getArrayResult();
    }

    public function save(User $user): void
    {
        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush();
    }

    public function update(): void
    {
        $this->getEntityManager()->flush();
    }

    public function delete(User $user): void
    {
        $this->getEntityManager()->remove($user);
        $this->getEntityManager()->flush();
    }
}
