<?php

declare(strict_types=1);

namespace Application\Repository;

use Application\Entity\Voivodeship;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;

class VoivodeshipRepository extends EntityRepository
{
    public function findByName(string $name)
    {
        return $this->createQueryBuilder('v')
            ->select('v')
            ->where('v.name = :name')
            ->setParameter('name', $name)
            ->getQuery()
            ->getOneOrNullResult(AbstractQuery::HYDRATE_ARRAY);
    }

    public function save(ArrayCollection $voivodeships): void
    {
        foreach ($voivodeships as $voivodeship) {
            $this->getEntityManager()->persist($voivodeship);
        }

        $this->getEntityManager()->flush();
    }
}
