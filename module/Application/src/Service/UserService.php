<?php

declare(strict_types=1);

namespace Application\Service;

use Application\Entity\Address;
use Application\Entity\AddressFactory;
use Application\Entity\User;
use Application\Entity\UserFactory;
use Application\Exception\UserNotFoundException;
use Application\Repository\UserRepository;
use Doctrine\ORM\NonUniqueResultException;

class UserService
{
    protected $userRepository;

    public function __construct(
        UserRepository $userRepository
    ) {
        $this->userRepository = $userRepository;
    }

    public function getById(int $id): array
    {
        try {
            return $this->userRepository->findOneById($id);
        } catch (\Exception $e) {
            throw new UserNotFoundException('User with id ' . $id . ' is not exists');
        }
    }

    public function getList(): array
    {
        return $this->userRepository->findAll();
    }

    public function save(
        string $name,
        string $email,
        ?string $voivodeship,
        ?string $street,
        ?string $houseNumber,
        ?string $city,
        ?string $postalCode,
        ?string $phonePrefix,
        ?string $phoneNumber,
        ?string $pesel,
        ?string $nip
    ): void
    {
        $user = (new UserFactory())->create(
            $name,
            $email,
            $voivodeship,
            $street,
            $houseNumber,
            $city,
            $postalCode,
            $phonePrefix,
            $phoneNumber,
            $pesel,
            $nip
        );

        try {
            $this->userRepository->save($user);
        } catch (\Exception $e) {
            throw new NonUniqueResultException('User with that email already exists');
        }
    }

    public function update(
        int $id,
        ?string $name,
        ?string $email,
        ?string $voivodeship,
        ?string $street,
        ?string $houseNumber,
        ?string $city,
        ?string $postalCode,
        ?string $phonePrefix,
        ?string $phoneNumber,
        ?string $pesel,
        ?string $nip
    ): void
    {
        $user = $this->userRepository->find($id);

        if (!$user)
        {
            throw new UserNotFoundException('User with id ' . $id . ' is not exists');
        }

        $address = $user->getAddress();

        if ($address)
        {
            $address->setVoivodeship($voivodeship);
            $address->setStreet($street);
            $address->setHouseNumber($houseNumber);
            $address->setCity($city);
            $address->setPostalCode($postalCode);
        } else {
            $address = (new AddressFactory())->create(
                $voivodeship,
                $street,
                $houseNumber,
                $city,
                $postalCode
            );
        }

        $address->setVoivodeship($voivodeship);
        $address->setStreet($street);
        $address->setHouseNumber($houseNumber);
        $address->setCity($city);
        $address->setPostalCode($postalCode);

        /**
         * @var User $user
         */
        $user->setName($name);
        $user->setEmail($email);
        $user->setAddres($address);
        $user->setPhonePrefix($phonePrefix);
        $user->setPhoneNumber($phoneNumber);
        $user->setPesel($pesel);
        $user->setNip($nip);

        try {
            $this->userRepository->update();
        } catch (\Exception $e) {
            throw new NonUniqueResultException('User with that email already exists');
        }
    }

    public function delete(int $id): void
    {
        $user = $this->userRepository->find($id);

        if (!$user)
        {
            throw new UserNotFoundException('User with id ' . $id . ' is not exists');
        }

        $this->userRepository->delete($user);
    }
}