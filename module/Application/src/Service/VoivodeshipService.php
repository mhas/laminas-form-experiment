<?php

namespace Application\Service;

use Application\Entity\Voivodeship;
use Application\Repository\VoivodeshipRepository;
use Doctrine\Common\Collections\ArrayCollection;

class VoivodeshipService
{
    protected $voivodeshipRepository;

    public function __construct(VoivodeshipRepository $voivodeshipRepository)
    {
        $this->voivodeshipRepository = $voivodeshipRepository;
    }

    public function synchronize(array $voivodeships)
    {
        $arrayCollection = new ArrayCollection();

        foreach ($voivodeships as $voivodeship) {
            if (!$this->voivodeshipRepository->findByName($voivodeship))
            {
                $arrayCollection->add(
                    new Voivodeship($voivodeship)
                );
            }
        }

        $this->voivodeshipRepository->save($arrayCollection);
    }
}