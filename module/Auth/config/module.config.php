<?php

declare(strict_types=1);

namespace Auth;

use Auth\Factory\AdapterHttpFactory;
use Laminas\Authentication\Adapter\Http as AdapterHttp;

return [
    'router' => [
        'routes' => [
        ],
    ],
    'controllers' => [
        'factories' => [

        ],
    ],
    'service_manager' => [
        'factories' => [
            AdapterHttp::class => AdapterHttpFactory::class
        ]
    ],
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
];
