<?php

namespace Auth\Factory;

use Interop\Container\ContainerInterface;
use Laminas\Authentication\Adapter\Http as AdapterHttp;
use Laminas\Authentication\Adapter\Http\FileResolver;
use Laminas\Config\Config;

class AdapterHttpFactory
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): AdapterHttp
    {
        $config = $container->get('Config');

        if (empty($config['auth'])) {
            throw new \LogicException('Missing auth configuration');
        }

        $adapterConfig = $config['auth'];

        $path = $config['auth']['basic_authentication_path'];
        $resolver = new FileResolver($path);
        $adapter = new AdapterHttp($adapterConfig);

        $adapter->setBasicResolver($resolver);

        return $adapter;
    }
}
