<?php

declare(strict_types=1);

namespace Auth;


use Laminas\Authentication\Adapter\Http as AdapterHttp;
use Laminas\ModuleManager\Feature\ConfigProviderInterface;
use Laminas\Mvc\MvcEvent;

class Module implements ConfigProviderInterface
{
    public function getConfig(): array
    {
        $config = include __DIR__ . '/../config/module.config.php';
        return $config;
    }

    public function onBootstrap(MvcEvent $event)
    {
        $application = $event->getApplication();
        $eventManager = $application->getEventManager();

        $eventManager->attach(
            MvcEvent::EVENT_DISPATCH,
            [$this, 'onRoute'],
            100
        );
    }

    public function onRoute(MvcEvent $event)
    {
        $application = $event->getApplication();
        $eventManager = $application->getEventManager();

        $adapter = $application->getServiceManager()->get(AdapterHttp::class);

        $adapter->setRequest($application->getRequest())->setResponse($application->getResponse());

        $result = $adapter->authenticate();

        if ($result->isValid())
        {
          return;
        }

        return $application->getResponse()->setContent('Invalid credentials');
    }
}