<?php

declare(strict_types=1);

namespace Blog;

use Blog\Controller\Api\CategoryController;
use Blog\Factory\CategoryControllerFactory;
use Blog\Factory\CategoryServiceFactory;
use Blog\Factory\PostControllerFactory;
use Blog\Controller\Api\PostController;
use Blog\Factory\PostInputFilterFactory;
use Blog\Factory\PostServiceFactory;
use Blog\Service\CategoryService;
use Blog\Service\PostService;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use Laminas\ServiceManager\Factory\InvokableFactory;

return [
    'router' => [
        'routes' => [
            'api' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/api',
                ],
                'may_terminate' => false,
                'child_routes' => [
                    'v1' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/v1',
                        ],
                        'may_terminate' => false,
                        'child_routes' => [
                            'posts' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/posts[/:id]',
                                    'defaults' => [
                                        'controller' => Controller\Api\PostController::class,
                                    ],
                                ],
                            ],
                            'categories' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/categories[/:id]',
                                    'defaults' => [
                                        'controller' => Controller\Api\CategoryController::class,
                                    ],
                                ],
                            ],
                        ]
                    ]
                ],
            ]
        ]
    ],
    'controllers' => [
        'factories' => [
            PostController::class => PostControllerFactory::class,
            CategoryController::class => CategoryControllerFactory::class
        ],
    ],
    'input_filters' => [
        'factories' => [
            InputFilter\SectionInputFilter::class => InvokableFactory::class,
            InputFilter\CategoryInputFilter::class => InvokableFactory::class,
            InputFilter\PostInputFilter::class => PostInputFilterFactory::class,
        ],
    ],
    'service_manager' => [
        'factories' => [
            PostService::class => PostServiceFactory::class,
            CategoryService::class => CategoryServiceFactory::class,
        ]
    ],
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ]
            ]
        ]
    ],
];
