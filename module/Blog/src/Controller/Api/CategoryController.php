<?php

namespace Blog\Controller\Api;

use Blog\InputFilter\CategoryInputFilter;
use Blog\InputFilter\PostInputFilter;
use Blog\Service\CategoryService;
use Blog\Service\PostService;
use Laminas\Mvc\Controller\AbstractRestfulController;
use Laminas\View\Model\JsonModel;

class CategoryController extends AbstractRestfulController
{
    private $categoryService;

    private $categoryInputFilter;

    public function __construct(CategoryService $categoryService, CategoryInputFilter $categoryInputFilter)
    {
        $this->categoryService = $categoryService;
        $this->categoryInputFilter = $categoryInputFilter;
    }

    public function get($id): JsonModel
    {
        return new JsonModel([
            'status' => 'success',
            'code' => 200,
            'data' => $this->categoryService->getOneById((int) $id)
        ]);
    }

    public function getList(): JsonModel
    {
        return new JsonModel([
            'status' => 'success',
            'code' => 200,
            'data' => $this->categoryService->getList()
        ]);
    }

    public function create($data): JsonModel
    {
        $this->categoryInputFilter->setData($data);

        if (!$this->categoryInputFilter->isValid())
        {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel([
                'status' => 'error',
                'code' => 400,
                'messages' => $this->categoryInputFilter->getMessages()
            ]);
        }

        $filteredData = $this->categoryInputFilter->getValues();

        $this->categoryService->save(
            $filteredData['name']
        );

        $this->getResponse()->setStatusCode(201);
        return new JsonModel([
            'status' => 'created',
            'code' => 201,
        ]);
    }

    public function update($id, $data): JsonModel
    {
        $this->categoryInputFilter->setData(array_merge($data, ['id' => $id]));

        if (!$this->categoryInputFilter->isValid())
        {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel([
                'status' => 'error',
                'code' => 400,
                'messages' => $this->categoryInputFilter->getMessages()
            ]);
        }

        $filteredData = $this->categoryInputFilter->getValues();

        $this->categoryService->update(
            (int) $id,
            $filteredData['name'],
        );

        $this->getResponse()->setStatusCode(201);
        return new JsonModel([
            'status' => 'updated',
            'code' => 201
        ]);
    }

    public function delete($id): JsonModel
    {
        $this->categoryService->delete((int) $id);

        $this->getResponse()->setStatusCode(200);
        return new JsonModel([
            'status' => 'success',
            'code' => 201,
        ]);
    }
}
