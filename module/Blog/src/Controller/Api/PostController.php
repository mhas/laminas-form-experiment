<?php

namespace Blog\Controller\Api;

use Blog\InputFilter\PostInputFilter;
use Blog\Service\PostService;
use Laminas\Mvc\Controller\AbstractRestfulController;
use Laminas\View\Model\JsonModel;

class PostController extends AbstractRestfulController
{
    private $postService;

    private $postInputFilter;

    public function __construct(PostService $postService, PostInputFilter $postInputFilter)
    {
        $this->postService = $postService;
        $this->postInputFilter = $postInputFilter;
    }

    public function get($id): JsonModel
    {
        return new JsonModel([
            'status' => 'success',
            'code' => 200,
            'data' => $this->postService->getOneById((int) $id)
        ]);
    }

    public function getList(): JsonModel
    {
        return new JsonModel([
            'status' => 'success',
            'code' => 200,
            'data' => $this->postService->getList()
        ]);
    }

    public function create($data): JsonModel
    {
        $this->postInputFilter->setData($data);

        if (!$this->postInputFilter->isValid())
        {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel([
                'status' => 'error',
                'code' => 400,
                'messages' => $this->postInputFilter->getMessages()
            ]);
        }

        $filteredData = $this->postInputFilter->getValues();

        $this->postService->save(
            $filteredData['title'],
            $filteredData['category'],
            $filteredData['sections'] ?? null
        );

        $this->getResponse()->setStatusCode(201);
        return new JsonModel([
            'status' => 'created',
            'code' => 201,
        ]);
    }

    public function update($id, $data): JsonModel
    {
        $this->postInputFilter->setData(array_merge($data, ['id' => $id]));

        if (!$this->postInputFilter->isValid())
        {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel([
                'status' => 'error',
                'code' => 400,
                'messages' => $this->postInputFilter->getMessages()
            ]);
        }

        $filteredData = $this->postInputFilter->getValues();

        $this->postService->update(
            (int) $id,
            $filteredData['title'],
            $filteredData['category'],
            $filteredData['sections'] ?? null
        );

        $this->getResponse()->setStatusCode(201);
        return new JsonModel([
            'status' => 'updated',
            'code' => 201
        ]);
    }

    public function delete($id)
    {
        $this->postService->delete((int) $id);

        $this->getResponse()->setStatusCode(200);
        return new JsonModel([
            'status' => 'success',
            'code' => 201,
        ]);
    }
}
