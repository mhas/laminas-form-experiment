<?php

namespace Blog\Entity;

use Doctrine\Common\Collections\ArrayCollection;

class CategoryFactory
{
    public function create(string $name): Category
    {
        return new Category($name);
    }
}