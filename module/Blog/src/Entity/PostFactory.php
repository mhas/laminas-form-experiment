<?php

namespace Blog\Entity;

use Doctrine\Common\Collections\ArrayCollection;

class PostFactory
{
    public function create(string $title, ?Category $category, ?array $sections): Post
    {
        $post = new Post($title, $category);

        $postSections = new ArrayCollection();
        if ($sections && count($sections)) {
            foreach ($sections as $section) {
                $postSections->add(new Section(
                    $section['title'],
                    $section['content'],
                    $section['order'] ?? null,
                ));
            }

            $post->setSections($postSections);
        }

        return $post;
    }
}