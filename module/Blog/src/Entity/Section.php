<?php

namespace Blog\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="section")
 * @ORM\Entity(repositoryClass="Blog\Repository\SectionRepository")
 */
class Section
{
    use IdTrait;

    /**
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @ORM\Column(type="string")
     */
    private $content;

    /**
     * @ORM\Column(name="'order'", type="integer", nullable=true)
     */
    private $order;

    public function __construct(string $title, string $content, ?int $order)
    {
        $this->title = $title;
        $this->content = $content;
        $this->order = $order;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    public function getOrder(): ?int
    {
        return $this->order;
    }

    public function setOrder(?int $order): void
    {
        $this->order = $order;
    }
}