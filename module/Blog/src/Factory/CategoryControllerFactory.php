<?php

declare(strict_types=1);

namespace Blog\Factory;


use Blog\Controller\Api\CategoryController;
use Blog\InputFilter\CategoryInputFilter;
use Blog\Service\CategoryService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class CategoryControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null): CategoryController
    {
        return new CategoryController(
            $container->get(CategoryService::class),
            $container->get('InputFilterManager')->get(CategoryInputFilter::class)
        );
    }

}
