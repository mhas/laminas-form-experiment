<?php

namespace Blog\Factory;


use Blog\Entity\Category;
use Blog\Entity\Post;
use Blog\Service\CategoryService;
use Doctrine\ORM\EntityManager;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;

class CategoryServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null): CategoryService
    {
        $entityManager = $container->get(EntityManager::class);

        return new CategoryService(
            $entityManager->getRepository(Category::class),
            $entityManager->getRepository(Post::class)
        );
    }
}
