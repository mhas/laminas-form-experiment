<?php

declare(strict_types=1);

namespace Blog\Factory;


use Blog\Controller\Api\PostController;
use Blog\InputFilter\PostInputFilter;
use Blog\Service\PostService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class PostControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null): PostController
    {
        return new PostController(
            $container->get(PostService::class),
            $container->get('InputFilterManager')->get(PostInputFilter::class)
        );
    }

}
