<?php

namespace Blog\Factory;

use Blog\InputFilter\CategoryInputFilter;
use Blog\InputFilter\PostInputFilter;
use Blog\InputFilter\SectionInputFilter;
use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class PostInputFilterFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null): PostInputFilter
    {
        return new PostInputFilter(
            $container->get('InputFilterManager')->get(SectionInputFilter::class),
            $container->get('InputFilterManager')->get(CategoryInputFilter::class)
        );
    }
}