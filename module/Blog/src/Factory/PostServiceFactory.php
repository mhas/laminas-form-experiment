<?php

namespace Blog\Factory;


use Blog\Entity\Category;
use Blog\Entity\Post;
use Blog\Entity\Section;
use Blog\Service\PostService;
use Doctrine\ORM\EntityManager;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;

class PostServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null): PostService
    {
        $entityManager = $container->get(EntityManager::class);

        return new PostService(
            $entityManager->getRepository(Post::class),
            $entityManager->getRepository(Section::class),
            $entityManager->getRepository(Category::class)
        );
    }
}
