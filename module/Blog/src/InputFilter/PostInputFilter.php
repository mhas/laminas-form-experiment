<?php

namespace Blog\InputFilter;

use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\Filter\ToInt;
use Laminas\InputFilter\CollectionInputFilter;
use Laminas\InputFilter\InputFilter;
use Laminas\Validator\GreaterThan;
use Laminas\Validator\StringLength;

class PostInputFilter extends InputFilter
{
    private $sectionInputFilter;
    private $categoryInputFilter;

    public function __construct(SectionInputFilter $sectionInputFilter, CategoryInputFilter $categoryInputFilter)
    {
        $this->sectionInputFilter = $sectionInputFilter;

        $this->categoryInputFilter = $categoryInputFilter;
    }

    public function init()
    {
        $sectionsCollectionInputFilter = new CollectionInputFilter();
        $sectionsCollectionInputFilter->setInputFilter(
            $this->sectionInputFilter
        );

        $categoryInputFilter = new InputFilter();
        $categoryInputFilter->add($this->categoryInputFilter);

        $this->add($sectionsCollectionInputFilter, 'sections');
        $this->add($categoryInputFilter, 'category');

        $this->add([
            'name' => 'title',
            'required' => true,
            'filters' => [
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 255
                    ]
                ],
            ],
        ]);

        $this->add([
            'name' => 'category',
            'required' => false,
            'filters' => [
                ['name' => ToInt::class],
            ],
            'validators' => [
                [
                    'name' => GreaterThan::class,
                    'options' => [
                        'min' => 0,
                        'inclusive' => true,
                    ]
                ],
            ],
        ]);
    }
}
