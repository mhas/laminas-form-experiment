<?php

namespace Blog\Repository;


use Blog\Entity\Category;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;

class CategoryRepository extends EntityRepository {

    public function findOneById(int $id): array
    {
        return $this->createQueryBuilder('c')
            ->select('c')
            ->where('p.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getSingleResult(AbstractQuery::HYDRATE_ARRAY);
    }

    public function save(Category $category): void
    {
        $this->getEntityManager()->persist($category);
        $this->getEntityManager()->flush();
    }

    public function update(): void
    {
        $this->getEntityManager()->flush();
    }

    public function delete(Category $category): void
    {
        $this->getEntityManager()->remove($category);
        $this->getEntityManager()->flush();
    }
}