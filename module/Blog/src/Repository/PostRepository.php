<?php

namespace Blog\Repository;

use Blog\Entity\Post;
use Blog\Entity\Section;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;

class PostRepository extends EntityRepository
{
    public function findOneById(int $id): array
    {
        return $this->createQueryBuilder('p')
            ->select('p', 's')
            ->leftJoin('p.sections', 's')
            ->where('p.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getSingleResult(AbstractQuery::HYDRATE_ARRAY);
    }

    public function findAllByCategoryId(int $id): array
    {
        return $this->createQueryBuilder('p')
            ->select('p')
            ->leftJoin('p.category', 'c')
            ->where('c.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
    }

    public function findAll(): array
    {
        return $this->createQueryBuilder('p')
            ->select('p', 's')
            ->leftJoin('p.sections', 's')
            ->getQuery()
            ->getArrayResult();
    }

    public function save(Post $post): void
    {
        foreach ($post->getSections() as $section) {
            $this->getEntityManager()->persist($section);
        }

        $this->getEntityManager()->persist($post);
        $this->getEntityManager()->flush();
    }

    public function update(Post $post): void
    {
        foreach ($post->getSections() as $section) {
            if (!$section->getId()) {
                $this->getEntityManager()->persist($section);
            }
        }

        $this->getEntityManager()->flush();
    }

    public function delete(Post $post): void
    {
        $this->getEntityManager()->remove($post);
        $this->getEntityManager()->flush();
    }
}