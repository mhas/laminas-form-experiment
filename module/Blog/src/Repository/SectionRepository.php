<?php

namespace Blog\Repository;

use Blog\Entity\Section;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;

class SectionRepository extends EntityRepository {

    public function delete(Section $section): void
    {
        $this->getEntityManager()->remove($section);
        $this->getEntityManager()->flush();
    }

    public function deleteCollection(Collection $sections): void
    {
        foreach ($sections as $section)
        {
            $this->getEntityManager()->remove($section);
        }

        $this->getEntityManager()->flush();
    }
}