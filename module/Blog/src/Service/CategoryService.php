<?php

namespace Blog\Service;


use Blog\Entity\CategoryFactory;
use Blog\Repository\CategoryRepository;
use Blog\Repository\PostRepository;

class CategoryService
{
    protected $categoryRepository;

    protected $postRepository;

    public function __construct(
        CategoryRepository $categoryRepository,
        PostRepository $postRepository
    )
    {
        $this->categoryRepository = $categoryRepository;
        $this->postRepository = $postRepository;
    }

    public function getList(): array
    {
        return $this->categoryRepository->findAll();
    }

    public function getOneById(int $id): array
    {
        try {
            return $this->categoryRepository->findOneById($id);
        } catch (\Exception $e) {
            throw new \Exception('Category with id: ' . $id . ' is not exists');
        }
    }

    public function save(string $name): void
    {
        $category = (new CategoryFactory())->create($name);

        $this->categoryRepository->save($category);
    }

    public function update(int $id, string $name): void
    {
        $category = $this->categoryRepository->find($id);

        if (!$category)
        {
            throw new \Exception('Category with id: ' . $id . ' is not exists');
        }

        $category->setName($name);

        $this->categoryRepository->update($category);
    }

    public function delete(int $id): void
    {
        $category = $this->categoryRepository->find($id);

        if (!$category)
        {
            throw new \Exception('Category with id: ' . $id . ' is not exists');
        }

        $posts = $this->postRepository->findAllByCategoryId($id);

        foreach ($posts as $post)
        {
            $post->setCategory(null);
        }

        $this->categoryRepository->delete($category);
    }
}
