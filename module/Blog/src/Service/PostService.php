<?php

namespace Blog\Service;



use Application\Exception\UserNotFoundException;
use Blog\Entity\Post;
use Blog\Entity\PostFactory;
use Blog\Entity\Section;
use Blog\Repository\CategoryRepository;
use Blog\Repository\PostRepository;
use Blog\Repository\SectionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class PostService
{
    protected $postRepository;

    protected $sectionRepository;

    protected $categoryRepository;

    public function __construct(
        PostRepository $postRepository,
        SectionRepository $sectionRepository,
        CategoryRepository $categoryRepository
    )
    {
        $this->postRepository = $postRepository;
        $this->sectionRepository = $sectionRepository;
        $this->categoryRepository = $categoryRepository;
    }

    public function getList(): array
    {
        return $this->postRepository->findAll();
    }

    public function getOneById(int $id): array
    {
        try {
            return $this->postRepository->findOneById($id);
        } catch (\Exception $e) {
            throw new \Exception('Post with id: ' . $id . ' is not exists');
        }
    }

    public function save(string $title, ?int $categoryId, ?array $sections)
    {
        $category = null;

        try {
            $category = $this->categoryRepository->find($categoryId);
        } catch (\Exception $exception){}

        $post = (new PostFactory())->create($title, $category, $sections);

        $this->postRepository->save($post);
    }

    public function update(int $id, string $title, ?int $categoryId, ?array $sections)
    {
        $category = null;

        try {
            $category = $this->categoryRepository->find($categoryId);
        } catch (\Exception $exception){}

        /**
         * @var Post $post
         */
        $post = $this->postRepository->find($id);

        if (!$post)
        {
            throw new \Exception('Post with id: ' . $id . ' is not exists');
        }

        $this->addSectionToPost($post, $sections);

        $post->setTitle($title);
        $post->setCategory($category);
        $post->setUpdatedAt(new \DateTime());

        $this->postRepository->update($post);
    }

    public function addSectionToPost(Post $post, $sections): void
    {
        $sectionsToSave = new ArrayCollection();
        $sectionsToDelete = new ArrayCollection();

            foreach($sections as $section) {
                if (!isset($section['id']))
                {
                    $sectionsToSave->add(new Section(
                        $section['title'],
                        $section['content'],
                        $section['order'] ?? null,
                    ));

                    continue;
                }

                $sectionIsExistsInPost = $post->getSections()->exists(function($key, $postSection) use ($section) {
                    return $section['id'] === $postSection->getId();
                });

                if (!$sectionIsExistsInPost) {
                    continue;
                }

                /**
                 * @var $postSection Section
                 */
                $postSection = $this->sectionRepository->find($section['id']) ?: null;

                if ($postSection)
                {
                    $postSection->setTitle($section['title']);
                    $postSection->setContent($section['content']);
                    $postSection->setOrder($section['order'] ?? null);

                    $sectionsToSave->add($postSection);
                }
            }

            foreach($post->getSections() as $section) {
                if (!$sectionsToSave->contains($section)) {
                    $sectionsToDelete->add($section);
                }
            }

        $this->sectionRepository->deleteCollection($sectionsToDelete);
        $post->setSections($sectionsToSave);
    }

    public function delete(int $id): void
    {
        $post = $this->postRepository->find($id);

        if (!$post)
        {
            throw new \Exception('Post with id ' . $id . ' is not exists');
        }

        $this->postRepository->delete($post);
    }
}


